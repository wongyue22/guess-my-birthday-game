from random import randint

name = input("Hi! What is your name?")
for guess_count in range(1, 6):
    month = randint(1, 12)
    year = randint(1924, 2004)

    print("Guess", guess_count, ":", name, "were you born in", month, "/", year, "?")
    answer = input("yes or no?")

    if guess_count < 5 and answer == "yes":
        print("I knew it!")
        exit()
    elif answer == "no":
        print("Drat! Lemme try again!")

    if guess_count == 5 and answer == "yes":
        print("I knew it!")
        exit()
    else:
        print("I have other things to do. Good bye.")
